import Backbone from 'backbone'
import { BackboneGsheets, columnIndexToA1range } from '../services/backbone-gsheets'
import { getPlanilhaConfig } from './PlanilhaConfig'

export class Inventario extends Backbone.Model {
    get sync() { return BackboneGsheets.sync(getPlanilhaConfig()) }
}

export class Inventarios extends Backbone.Collection {
    get model() { return Inventario }

    get sync() { return BackboneGsheets.sync(getPlanilhaConfig()) }

    constructor(...props) {
        super(...props)
        this.filters = {}
        this.loadFilters()
    }

    fetch({...options}) {
        const where = []
        if(this.search) where.push(`A like '${this.search}%'`)
        Object.keys(this.filters).forEach(field => {
            const columnMap = getPlanilhaConfig().columnMap
            const col = columnIndexToA1range(columnMap[field])
            const number = Number(this.filters[field])
            if(Number.isNaN(number)) {
                where.push(`${col} = '${this.filters[field]}'`)
            } else if(this.filters[field] == null) {
                where.push(`${col} is null`)
            } else {
                where.push(`${col} = ${this.filters[field]}`)
            }
        })
        if(where.length > 0) {
            options.query = `select * where ${where.join(" and ")}`
        }
        return super.fetch(options)
    }

    filterBy(field, value) {
        this.filters[field] = value
        this.saveFilter()
    }

    notFilterBy(field) {
        delete this.filters[field]
        this.saveFilter()
    }

    saveFilter() {
        window.sessionStorage.setItem(
            "inventarios.filters",
            JSON.stringify(this.filters)
        )
    }

    clearFilter() {
        window.sessionStorage.removeItem(
            "inventarios.filters"
        )
    }

    loadFilters() {
        try {
            this.filters = JSON.parse(window.sessionStorage.getItem(
                "inventarios.filters"
            )) || {}
        } catch {
            this.filters = {}
        }
    }
}

export default {Inventario, Inventarios}
