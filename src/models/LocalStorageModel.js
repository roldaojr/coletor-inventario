import Backbone from 'backbone'

export default class LocalStorageModel extends Backbone.Model {
    constructor(key) {
        super()
        this.key = key
        this.defaults = {}
    }

    async fetch() {
        try {
            const attributes = JSON.parse(window.localStorage.getItem(
                this.key
            ))
            this.attributes = {...this.defaults, ...attributes}
            this.trigger("read", this)
            this.trigger("change", this)
        } catch (ex) {
            this.trigger("error", this, ex)
        } finally {
            this.trigger("sync", this)
        }
    }

    async save() {
        window.localStorage.setItem(
            this.key,
            JSON.stringify(this.attributes)
        )
    }

    async destroy() {
        window.localStorage.removeItem(this.key)
        this.trigger("delete", this)
    }
}
