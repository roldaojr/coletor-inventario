import Backbone from 'backbone'

export default class AsyncCollection extends Backbone.Collection {
    constructor(values) {
        super()
        if(values) this.set(values)
    }

    async set(values) {
        const models = await Promise.resolve(values)
        this.models = models
        this.trigger("reset", this)
        this.trigger("sync", this)
    }
}
