import Backbone from 'backbone'
import BackboneGsheets from '../services/backbone-gsheets'

export default class Responsaveis extends Backbone.Collection {
    get sync() { return BackboneGsheets.sync({
        spreadsheetId: "1kX3e38BEKgB4gNPvqKILcy3zAyjTLoRpMxabzCr84_Y",
        sheet: "Itens"
    }) }

    fetch() {
        return super.fetch({
            formula: "sort(unique(Itens!G2:G))",
            columnMap: {nome: 1}
        })
    }
}
