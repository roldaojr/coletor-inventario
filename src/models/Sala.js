import Backbone from 'backbone'
import BackboneGsheets from '../services/backbone-gsheets'
import { getPlanilhaConfig } from './PlanilhaConfig'

export class Salas extends Backbone.Collection {
    get sync() {
        const config = getPlanilhaConfig()
        config.columnMap = {
            "nome": 1,
            "total": 2,
            "encontrados": 3,
        }
        config.query = "select E, count(E), count(H) group by E order by E"
        return BackboneGsheets.sync(config)
    }
}

export default Salas
