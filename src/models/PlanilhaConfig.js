import LocalStorageModel from './LocalStorageModel'
import { getGoogleSheetsService } from '../services/GoogleSheetsService'

export class LocalStorageJson {
    constructor(prefix) {
        this.prefix = prefix
    }

    get(key) {
        try {
            return JSON.parse(window.localStorage.getItem(`${this.prefix}.${key}`))
        } catch {
            return null
        }
    }

    set(key, value) {
        window.localStorage.setItem(`${this.prefix}.${key}`, JSON.stringify(value))        
    }

    delete(key) {
        window.localStorage.removeItem(`${this.prefix}.${key}`)
    }
}

export default class PlanilhaConfig extends LocalStorageModel {
    constructor() {
        super('planilha')
        this.isSignedIn = false
        this.auth = null
        this.service = getGoogleSheetsService()
        this.cache = new LocalStorageJson('planilha.cache')
        this.on('change:arquivo', () => this.cache.delete("planilhas"))
        this.on('change:planilha', () => this.cache.delete("colunas"))
    }

    async authorize() {
        if(this.auth) return this.auth
        this.auth = await this.service.authorize()
        this.auth.isSignedIn.listen(status => {
            this.trigger("change:isSignedIn", status)
            this.isSignedIn = status
        })
        this.trigger("change:isSignedIn", this.auth.isSignedIn.get())
        return this.auth
    }

    get isConfigured() {
        const {spreadsheetId, sheet, numero} = this.attributes
        return spreadsheetId && sheet && numero
    }

    async getUser() {
        const drive = await this.service.getDriveApi()
        const response = await drive.about.get({'fields': 'user'})
        return response.result.user
    }

    async fetch_arquivos() {
        console.debug('Listar arquivos')
        try {
            let response = await this.service.listSpreadsheetFiles()
            return response.files
        }
        catch (err) {
            $.mobile.toast({message: "Erro ao acessar API do Gooogle"})
            console.error(err)
        }
        return []
    }

    async getPlanilhas() {
        if(!this.attributes.spreadsheetId) return []
        const cache = this.cache.get("planilhas")
        if(cache) { 
            console.debug(`Planilhas (em cache) do arquivo ${this.attributes.spreadsheetId}`)
            return cache
        }
        try {
            console.debug(`Planilhas do arquivo ${this.attributes.spreadsheetId}`)
            let response = await this.service.listFileSheets(this.attributes.spreadsheetId)
            this.cache.set("planilhas", response)
            return response
        } catch(err) {
            $.mobile.toast({message: "Erro ao acessar API do Gooogle"})
            console.error(err.details)
        }
    }

    async getColunas() {
        if(!this.attributes.sheet) return []
        const cache = this.cache.get("colunas")
        if(cache) { 
            console.debug(`Colunas (em cache) da planilha ${this.attributes.sheet}`)
            return cache
        }
        console.debug(`Colunas da planilha ${this.attributes.sheet}`)
        let colunas = await this.service.fetchCells(
            this.attributes.spreadsheetId, `${this.attributes.sheet}!1:1`, 'ROWS'
        )
        if (colunas.length > 0) {
            colunas = colunas[0]
            this.cache.set("colunas", colunas)
        } else {
            colunas = []
        }
        return colunas
    }
}

export const getPlanilhaConfig = () => {
    const config = new LocalStorageModel('planilha')
    config.fetch()
    const {spreadsheetId, sheet, ...columnMap} = config.attributes
    if(!spreadsheetId || !sheet || !columnMap) {
        window.location.href = "/#config"
        console.info("Configuração de planilha necessária")
        $.mobile.toast({message: "Configuração de planilha necessária"})
    }
    return {spreadsheetId, sheet, columnMap}
}
