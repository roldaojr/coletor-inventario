import LocalStorageModel from './LocalStorageModel'

export default class Settings extends LocalStorageModel {
    constructor() {
        super("settings")
        this.defaults = {
            database: "inventarios"
        }
    }
}
