import Dialog from './Dialog'

export default class SalaFilterDialog extends Dialog {
    get events() {
        return {
            "submit #filtrar-sala-form": this.filtrar.bind(this)
        }
    }
    constructor() {
        super({
            id: "filtrar-sala-dialog",
            title: "Filtrar sala",
            children: [
                `<form id="filtrar-sala-form">
                    <label for="filtro-sala">Sala</label>
                    <div class="ui-input-text ui-corner-all ui-shadow-inset">
                        <input type="text" name="filtro-sala" data-theme="a">
                    </div>                       
                    <button type="submit" class="ui-btn ui-mini ui-corner-all ui-btn-b">Confirmar</button>
                </form>`
            ]
        })
        this.returnValue = null
    }

    filtrar(e) {
        e.preventDefault()
        this.returnValue = $(e.target).find("[name=filtro-sala]").val()
        this.$el.popup("close");
    }

    async show() {
        return new Promise(resolve => {
            super.show()
            this.$el.on("popupafterclose", () => {
                resolve(this.returnValue)
            })
        })
    }
}