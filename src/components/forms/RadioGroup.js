import Backbone from 'backbone'

export default class RadioGroup extends Backbone.View {
    constructor({options, ...props}) {
        super()
        this.props = props
        this.options = options || []
        this.$el.addClass("ui-field-contain")
    }

    setModel(model) {
        this.model = model
        this.record = model.attributes
        this.listenTo(model, "change", this.modelUpdateListener)
    }

    modelUpdateListener(model) {
        this.render()
        this.$el.find("[data-role=controlgroup]").controlgroup({enhanced: true})
        this.$el.find("input[type=radio]").checkboxradio({enhanced: true})
    }

    render() {
        this.$el.html(`
        <fieldset data-role="controlgroup" data-type="horizontal"
        class="ui-controlgroup ui-controlgroup-horizontal ui-corner-all"
        style="borderStyle: none" data-enhanced="true">
            <div role="heading" class="ui-controlgroup-label">
                <legend>
                    ${this.props.label}
                </legend>
            </div>
            <div class="ui-controlgroup-controls">
                ${Object.keys(this.options).map((option, idx) => {
                    let option_id = `${this.props.name}_${idx}`
                    let selected = (this.model.get(this.props.name) == option)
                    let radio_classes = "ui-btn ui-corner-all ui-btn-inherit ui-btn-icon-left"
                    radio_classes += (selected)?" ui-radio-on":" ui-radio-off"        
                    return (`
                        <div class="ui-radio">
                            <label class="${radio_classes}" for="${option_id}">
                                ${this.options[option]}
                            </label>
                            <input type="radio" name="${this.props.name}" id="${option_id}" value="${option}" ${(selected)?"checked":""}>
                        </div>
                    `)
                }).join("")}
            </div>
        </fieldset>
        `)
        this.$el.attr("id", `field_${this.props.name}`)
        this.$el.find("input[type=radio]").checkboxradio({enhanced: true})
    }
}
