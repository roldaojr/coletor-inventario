import Backbone from 'backbone'

export default class TextInput extends Backbone.View {
    constructor({...props}) {
        super()
        this.props = props
        this.$el.addClass("ui-field-contain")
    }

    setModel(model) {
        this.model = model
        this.listenTo(
            this.model,
            "change",
            this.modelUpdateListener.bind(this)
        )
    }

    modelUpdateListener() {
        this.$el.find("input").val(this.model.get(this.props.name))
    }

    render() {        
        this.$el.html(`
        <label for="id_${this.props.name}">
            ${this.props.label}
        </label>
        <div class="ui-input-text ui-body-inherit ui-corner-all ui-shadow-inset">
            <input type="text" data-enhanced="true" name="${this.props.name}"
            value="${this.model.get(this.props.name) || ""}" ${(this.props.readonly)?"readonly":""}>
        </div>
        `)
        this.$el.attr("id", `field_${this.props.name}`)
        this.$el.find("input").on('change', e => {
            if(!this.props.readonly) this.model.set(this.props.name, e.target.value)
        })
        return this
    }
}
