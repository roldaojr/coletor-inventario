import TextInput from './TextInput'

export default class TextInputWithButton extends TextInput {
    render() {
        this.$el.html(`
        <label for="id_${this.props.name}">
            ${this.props.label}
        </label>
        <div class="ui-input-text ui-body-inherit ui-corner-all ui-shadow-inset">
            <button type="button" class="ui-btn ui-btn-icon-notext ${
                this.props.button_class || "ui-btn-c ui-icon-info"
            }"
            style="margin: 1px 0 0 2px; float: right; width: 32px; height: 32px"></button>
            <input type="text" data-enhanced="true" name="${this.props.name}"
            style="width: calc(100% - 36px)" value="${this.model.get(this.props.name) || ""}"
            ${(this.props.readonly)?"readonly":""}>
        </div>
        `)
        this.$el.attr("id", `field_${this.props.name}`)
        this.$el.find("input").on('change', e => {
            if(!this.props.readonly) this.model.set(this.props.name, e.target.value)
        })
    }
}