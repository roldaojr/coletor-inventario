import Backbone from 'backbone'

export default class TextInput extends Backbone.View {
    constructor({...props}) {
        super()
        this.props = props
        this.$el.addClass("ui-checkbox")
    }

    setModel(model) {
        this.model = model
        this.listenTo(
            this.model,
            "change",
            this.modelUpdateListener.bind(this)
        )
    }

    modelUpdateListener() {
        this.$el.find("input").attr("checked", this.model.get(this.props.name))
        //this.$el.checkboxradio({enhanced: true})
    }

    render() {
        let checked = this.model.get(this.props.name)
        this.$el.html(`
            <label for="checkbox-enhanced" class="ui-btn ui-corner-all ui-btn-inherit ui-btn-icon-left ui-checkbox-off ui-btn-c">
                ${this.props.label}
            </label>
            <input type="checkbox" name="${this.props.name}" data-enhanced="true" ${(checked)?"checked":""} ${(this.props.readonly)?"readonly":""}>
        `)
        this.$el.find("input").on('change', e => {
            if(!this.props.readonly) this.model.set(this.props.name, e.target.checked)
        })
    }
}
