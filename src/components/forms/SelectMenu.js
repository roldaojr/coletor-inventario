import Backbone from 'backbone'

export default class SelectMenu extends Backbone.View {
    constructor({options, ...props}) {
        super()
        this.props = props
        if(options instanceof Backbone.Collection) {
            this.options = options
            this.listenTo(this.options, "reset sync", this.modelUpdateListener)
        } else {
            this.options = options || []
        }
        this.$el.addClass("ui-field-contain")
    }

    setModel(model) {
        this.model = model
        this.listenTo(model, "change", this.modelUpdateListener)
    }

    modelUpdateListener() {
        this.render()
        this.$el.find("select").selectmenu({enhanced: true})
    }

    render() {
        let object_list
        if(this.options instanceof Backbone.Collection) {
            object_list = this.options.models
        } else {
            object_list = Object.keys(this.options).map(it => {
                return {key: it, value: this.options[it]}
            })
        }
        this.$el.html(`
        <label>
            ${this.props.label}
        </label>
        <select name=${this.props.name} data-theme="c">
        ${object_list.map(({key, value}) => {
            let selected = (this.model && key == this.model.get(this.props.name))
            return (`<option value="${key}" ${(selected)?"selected":""}>
                ${value}
            </option>`)
        })}
        </select>
        `)
        this.$el.attr("id", `field_${this.props.name}`)
        this.$el.find("select").on('change', e => {
            if(!this.props.readonly) this.model.set(this.props.name, e.target.value)
            if(this.props.onChange) this.props.onChange(e)
        })
        return this
    }
}
