import Backbone from 'backbone'

export default class TextArea extends Backbone.View {
    constructor({...props}) {
        super()
        this.props = props
        this.$el.addClass("ui-field-contain")
    }

    setModel(model) {
        this.model = model
        this.record = model.attributes
        this.listenTo(model, "change", this.modelUpdateListener)
    }

    modelUpdateListener(model) {
        this.$el.find("textarea").val(model.get(this.props.name))
    }

    render() {
        this.$el.html(`
        <label for="id_${this.props.name}">
            ${this.props.label}
        </label>
        <textarea class="ui-input-text ui-shadow-inset ui-body-inherit ui-corner-all ui-textinput-autogrow"
        data-enhanced="true" name="${this.props.name}" ${(this.props.readonly)?"readonly":""}
        >${this.record[this.props.name] || ""}</textarea>
        `)
        this.$el.attr("id", `field_${this.props.name}`)
        return this
    }
}
