import Backbone from 'backbone'

export default class Panel extends Backbone.View {
    constructor({ children, ...props }) {
        super()
        this.$el.attr("data-role", "panel")
        this.$el.attr("data-display", props.display || "overlay")
        this.$el.attr("data-theme", props.theme || "a")
        this.$el.attr("data-position", props.position || "left")
        this.$el.attr("data-dismissible", "true")
        this.$el.attr("data-position-fixed", props.position || "true")
        this.$el.attr("id", props.id)
        if(children && !Array.isArray(children)) {
            this.children = [children]
        } else {
            this.children = children
        }
    }

    render() {
        let $content = $(`<div class="ui-panel-inner"></div>`).appendTo(this.$el)
        let children = this.children || []
        children.forEach(it => {
            if(typeof it == "object" && "render" in it) {
                it.render()
                return $content.append(it.el)
            } else {
                return $content.append(it.toString())
            }
        })
        this.$el.panel({
            dismissible: true
        })
        return this
    }
}
