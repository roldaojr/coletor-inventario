import Backbone from 'backbone'

export default class Form extends Backbone.View {
    get tagName() { return 'form' }

    constructor({children, model, id, ...props}) {
        super()
        this.props = props
        this.children = children
        this.model = model
        this.children.forEach(field => {
            if(field.setModel) {
                field.setModel(model)
            } else {
                field.model = model
                field.record = model.attributes
            }
        })
        this.$el.attr("id", id)
        if(props.onSubmit) {
            this.$el.on("submit", props.onSubmit)
        } else {
            this.$el.on("submit", this.submit.bind(this))
        }
    }

    submit(e) {
        console.debug("Saving...")
        e.preventDefault()
        this.model.save()
    }

    render() {
        this.$el.html("")
        this.children.forEach(it => {
            if("render" in it) {
                it.render()
                return this.$el.append(it.el)
            } else {
                return this.$el.append(it.toString())
            }
        })
        this.$el.append(
            `<button type="submit" class="ui-btn ui-btn-b ui-corner-all">
                ${this.props.submiButtonLabel || "Salvar"}
            </button>`
        )
        return this
    }
}
