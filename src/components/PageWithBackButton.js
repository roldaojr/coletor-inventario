import Page from './Page'

export default class PageWithBackButton extends Page {
    get events() { return {
        "click #back-button": () => window.history.go(-1)
    }}

    constructor(props) {
        props.leftButton = `
        <a id="back-button" data-rel="back" data-role="button" role="button" class="ui-btn-left ui-btn ui-btn-icon-notext ui-corner-all ui-icon-carat-l">
            Voltar
        </a>`
        super(props)
    }
}
