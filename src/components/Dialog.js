import Backbone from 'backbone'

export default class Dialog extends Backbone.View {
    constructor({children, ...props}) {
        super()
        this.props = props
        this.children = children || []
    }

    componentTemplate() {
        return `
        <div data-role="header" data-theme="${this.props.theme || "a"}" role="banner"
        class="ui-header ui-bar-${this.props.theme || "a"}">
            <h1 class="ui-title" role="heading" aria-level="1">${this.props.title}</h1>
        </div>
        <div data-role="content" class="ui-content"></div>`
    }

    render() {
        this.$el.attr({
            "id": this.props.id,
            "class": "ui-popup ui-body-b ui-overlay-shadow ui-corner-all",
            "data-role": "popup",
            "data-theme": this.props.theme || "b",
            "data-overlay-theme": this.props.overlay || "a",
            "data-dismissible": this.props.dismissible || true,
        })
        this.$el.on("popupafterclose", () => this.$el.remove())
        this.$el.html(this.componentTemplate())
        let $content = this.$el.find("[data-role=content]")
        this.children.forEach(child => {
            if(child instanceof Backbone.View) {
                child.render()
                $content.append(child.el)
            } else {
                $content.append(child)
            }
        })
        return this
    }

    show() {
        console.debug("show dialog")
        this.render().$el.appendTo(document.body)
        this.$el.popup()
        this.$el.popup("open")
    }
}