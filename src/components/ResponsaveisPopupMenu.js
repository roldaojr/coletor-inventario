import ListView from './ListView'
import Responsaveis from '../models/Responsavel'
import Popup from './Popup'
import { BackboneAsync } from '../services/AsyncIndicator'

export default class ResponsaveisPopupMenu extends Popup {
    get events() {
        return {
            "click li a": this.selectItem.bind(this),
            ...super.events
        }
    }
    constructor() {
        super({
            id: "responsaveis-menu",
            transition: "pop",
            title: "Responsáveis"
        })
        this.returnValue = null
        this.responsaveis = new Responsaveis()
        this.children = [
            new ListView({
                collection: this.responsaveis,
                itemTemplate: i => `<a>${i.attributes.nome}</a>`
            })
        ]
    }

    selectItem(e) {
        e.preventDefault()
        this.returnValue = $(e.target).text()
        this.$el.popup("close")
    }

    async show() {
        this.responsaveis.fetch()
        await BackboneAsync(this.responsaveis, this)
        return new Promise(resolve => {
            super.show()
            this.$el.on("popupafterclose", () => {
                resolve(this.returnValue)
            })
        })
    }
}