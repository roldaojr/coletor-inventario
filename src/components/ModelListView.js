import Backbone from 'backbone'

let defaultItemTemplate = i => `<a>${i}</a>`

export default class ListView extends Backbone.View {
    get tagName() { return 'ul' }

    constructor({collection, ...props}) {
        super()
        this.itemTemplate = props.itemTemplate || defaultItemTemplate
        this.collection = collection

        this.$el.attr("data-role", "listview")
        this.$el.attr("data-theme", "c")
        this.$el.addClass("ui-listview ui-group-theme-c")
        this.listenTo(
        	this.collection,
        	"change reset add remove",
        	this.render
        )
    }

    render() {
        this.$el.html(this.collection.models.map(it => (
            `<li>${this.itemTemplate(it)}</li>`
        )).join(""))
        this.$el.listview()
        this.$el.listview("refresh")
    	return this
    }
}
