import Backbone from 'backbone'

const defaultItemTemplate = i => `<a>${i}</a>`

export default class ListView extends Backbone.View {
    get tagName() { return 'ul' }

    constructor({...props}) {
        super()
        this.itemTemplate = props.itemTemplate || defaultItemTemplate

        this.$el.attr("data-role", "listview")
        this.$el.attr("data-theme", props.theme || "c")
        this.$el.attr("data-split-theme", props.theme || "c")
        this.$el.attr("data-inset", props.inset || "false")
        this.$el.addClass("ui-listview ui-group-theme-"+props.theme || "c")

        if(props.collection instanceof Backbone.Collection) {
            this.collection = props.collection
            this.listenTo(this.collection, "sync", this._onCollectionSync)
        } else {
            this.items = props.items
        }
    }

    _onCollectionSync(collection) {
        this.$el.html(collection.models.map((it, i) => (
            `<li>${this.itemTemplate(it, i)}</li>`
        )).join(""))
        return this.$el.listview().listview('refresh')
    }

    render() {
        let object_list
        if(this.collection instanceof Backbone.Collection) {
            object_list = this.collection.models
        } else {
            object_list = this.items
        }
        this.$el.html(object_list.map((it, i) => (
            `<li>${this.itemTemplate(it, i)}</li>`
        )).join(""))
        return this.$el.listview().listview('refresh')
    }
}
