import Backbone from 'backbone'

export default class FooterPager extends Backbone.View {
    constructor(collection) {
        super()
        this.$el.attr("data-role", "footer")
        this.$el.attr("data-theme", "a")
        this.$el.attr("data-position", "fixed")

        this.collection = collection
        this.listenTo(
            this.collection,
            "change reset add remove",
            this.update
        )
    }

    get events() {
        return {
            "click #prev-page": this.prevPage,
            "click #next-page": this.nextPage
        }
    }

    prevPage() {
        if(this.collection.currentPage > 1) {
            this.collection.currentPage--
            this.collection.fetch()
        }
    }

    nextPage() {
        if(this.collection.currentPage < this.collection.totalPages) {
            this.collection.currentPage++
            this.collection.fetch()
        }
    }

    update() {
        this.$el.find("h4").text(`Pagina ${this.collection.currentPage}/${this.collection.totalPages}`)
    }

    render() {
        this.$el.html(`
            <div data-role="header">
                <button id="prev-page" class="ui-btn-left ui-nodisc-icon ui-btn ui-icon-carat-l ui-btn-icon-notext ui-corner-all"></button>
                <h4></h4>
                <button id="next-page" class="ui-btn-right ui-nodisc-icon ui-btn ui-icon-carat-r ui-btn-icon-notext ui-corner-all"></button>
            </div>`)
        return this
    }
}
