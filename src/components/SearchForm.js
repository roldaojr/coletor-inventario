import Backbone from 'backbone'
import { BackboneAsync } from '../services/AsyncIndicator'

export default class SearchForm extends Backbone.View {
    get tagName() { return "form" }

    get events() { return { "keyup input": this.searchfilter }}

    constructor(collection) {
        super()
        this.$el.attr("id", "searchorm")
        this.$el.attr("style", "margin: -0.5em 0.1em -0.5em 0")
        this.collection = collection
    }

    searchfilter(e) {
        e.preventDefault()
        if (e.keyCode == 13) {
            const searchText = this.$el.find("input").val()
            this.collection.search = searchText
            this.collection.fetch()
            BackboneAsync(this.collection, this)
        }
    }

    render() {
        this.$el.html(`
            <div class="ui-input-search ui-body-inherit ui-corner-all ui-shadow-inset ui-input-has-clear">
                <input data-type="search" autocomplete="off" data-clear-btn="true" data-enhanced="true">
            </div>
        `)
        return this
    }
}
