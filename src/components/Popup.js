import Backbone from 'backbone'

export default class Popup extends Backbone.View {
    constructor({children, id, ...props}) {
        super()
        this.props = props
        this.children = children || []
        Object.keys(this.props).forEach(key => {
            this.$el.attr(`data-${key}`, this.props[key])
        })
        this.$el.attr({
            id, "data-role": "popup",
            "class": "ui-popup ui-body-b ui-overlay-shadow ui-corner-all",
            "data-theme": this.props.theme || "b",
            "data-overlay-theme": this.props.overlay || "a",
            "data-dismissible": this.props.dismissible || true,
        })
    }

    render() {
        this.$el.on("popupafterclose", () => this.$el.remove())
        this.children.forEach(child => {
            if(child instanceof Backbone.View) {
                child.render()
                this.$el.append(child.el)
            } else {
                this.$el.append(child)
            }
        })
        return this
    }

    async show() {
        this.render().$el.appendTo(document.body)
        this.$el.popup()
        this.$el.popup("open")
    }
}
