import Page from './Page'

export default class PageWithMenu extends Page {
    constructor(props) {
        props.leftButton = `
        <a href="#navpanel" data-icon="bars" data-iconpos="notext">
            Menu
        </a>`
        super(props)
    }
}
