import Backbone from 'backbone'

export default class Page extends Backbone.View {
    constructor({children, ...props}) {
        super()
        this.props = props
        if(Array.isArray(children)) {
            this.children = children
        } else {
            this.children = [children]
        }
    }

    pageTemplate() {
        return `
        <div data-role="header" data-theme="a" data-position="fixed">
            ${this.props.leftButton || ""}
            <h1>${this.props.title}</h1>
            ${this.props.rightButton || ""}
        </div>
        <div data-role="content"></div>`
    }

    render() {
        this.$el.html(this.pageTemplate())
        let $content = this.$el.find("[data-role=content]")
        this.children.forEach(child => {
            if(child instanceof Backbone.View) {
                child.render()
                $content.append(child.el)
            } else {
                $content.append(child)
            }
        })
        return this
    }
}
