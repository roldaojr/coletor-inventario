import Page from '../components/Page'
import ListView from '../components/ListView'
import SearchForm from '../components/SearchForm'
import { Inventarios } from '../models/Inventario'
import FilterPanel from './FilterPanel'
import { BackboneAsync } from '../services/AsyncIndicator'

export default class ListarPage extends Page {
    get events() { return {
        "click #load-more-up": "loadMoreUpClick",
        "click #load-more-down": "loadMoreDownClick"
    } }

    constructor() {
        super({
            title: "Itens",
            leftButton: `<a href="#navpanel" data-icon="bars" data-iconpos="notext">Menu</a>`,
            rightButton: `<a href="#filter-panel" data-icon="gear" data-iconpos="notext">Filter</a>`
        })
        this.inventarios = new Inventarios()
        this.searchForm = new SearchForm(this.inventarios)
        this.listviewOffset = 0
        this.pageSize = 20

        let itemTemplate = (item) => {
            return `
            <a href="#editar/${item.get("id")}">
                ${item.attributes.numero}<br>
                ${item.attributes.descricao}<br>
                ${(item.attributes.sala_atual) ? 'Encontrado: '+item.attributes.sala_atual : "Não encontrado"}
            </a>`
        }
        this.listview = new ListView({
            collection: this.inventarios,
            itemTemplate: itemTemplate
        })
        this.children = [
            `<div id="div-load-more-up" style="margin: -1em -1em 1em -1em; display: none">
                <button style="margin: 0" id="load-more-up" class="ui-btn ui-btn-c">Carregar mais</button>
            </div>`,
            this.listview,
            `<div id="div-load-more-down" style="margin: 1em -1em -1em -1em; display: none">
                <button style="margin: 0" id="load-more-down" class="ui-btn ui-btn-c">Carregar mais</button>
            </div>`,
        ]
        this.filterPanel = new FilterPanel({parent: this})
        console.debug("Listando itens com o filtro", this.inventarios.filters)
        this.loadItens()
    }

    async loadItens() {
        this.inventarios.fetch({
            offset: this.listviewOffset || 0,
            limit: this.pageSize,
        })
        return BackboneAsync(this.inventarios, this).then(collection => {
            if(this.listviewOffset > 0) {
                $("#div-load-more-up").show()
            } else {
                $("#div-load-more-up").hide()
            }
            if(collection.models.length == this.pageSize) {
                $("#div-load-more-down").show()
            } else {
                $("#div-load-more-down").hide()
            }
        })
    }

    filterDisplayText() {
        const filtros = this.inventarios.filters
        const texto = []
        if(filtros.sala_suap) {
            texto.push(filtros.sala_suap)
        }
        if(filtros.hasOwnProperty("sala_atual")) {
            if(filtros.sala_atual) {
                texto.push(`Encontrado: ${filtros.sala_atual}`)
            } else {
                texto.push("Não encontrados")
            }    
        }
        if(filtros.responsavel_busca) {
            texto.push(filtros.responsavel_busca)
        }
        return texto.join("<br>")
    }

    
    async loadMoreUpClick() {
        if(this.listviewOffset >= 10) {
            this.listviewOffset -= 10
        } else {
            this.listviewOffset = 0
        }
        await this.loadItens()
        $(window).scrollTop($("#load-more-up").outerHeight())
    }

    async loadMoreDownClick() {
        this.listviewOffset += 10
        await this.loadItens()
        $(window).scrollTop($("#load-more-up").outerHeight())
    }

    render() {
        super.render()
        let $header = this.$el.find("[data-role=header]")
        this.searchForm.render().$el.appendTo($header)
        this.filterPanel.render().$el.appendTo(this.$el)
        return this
    }

    pageTemplate() {
        return `
        <div data-role="header" data-theme="a" data-position="fixed">
            ${this.props.leftButton || ""}
            <h1>${this.props.title}</h1>
            ${this.props.rightButton || ""}
        </div>
        <div data-role="content"></div>
        <div data-role="footer" data-theme="a" data-position="fixed">
            <nobr>${this.filterDisplayText()}</nobr>
        </div>`
    }
}
