import Page from '../components/PageWithMenu'
import Form from '../components/Form'
import TextImput from '../components/forms/TextInput'
import TextInputWithButton from '../components/forms/TextInputWithButton'
import SelectMenu from '../components/forms/SelectMenu'
import RadioGroup from '../components/forms/RadioGroup'
import { Inventario } from '../models/Inventario'
import AsyncIndicator, { BackboneAsync } from '../services/AsyncIndicator'

let Field = e => e
let situacao_opts = {
    'Bom': 'Bom',
    'Ocioso': 'Ocioso',
    'Recuperável': 'Recuperável',
    'Irrecuperável': 'Irrecuperável',
    'Antieconomico': 'Antieconomico'
}
let etiqueta_opts = {'Boa': 'Boa', 'Necessaria': 'Necessaria'}

export default class Coletar extends Page {
    constructor() {
        super({title: "Coletar"})
        let model = new Inventario({
            situacao: "Bom",
            etiqueta: "Boa",
            sala_atual: window.localStorage.getItem("coletar.sala")
        })
        this.listenTo(model, "change:sala_atual", () => {
            window.localStorage.setItem(
                "coletar.sala",
                model.get("sala_atual")
            )
        })
        this.form = new Form({
            model: model,
            children: [
                new TextImput({
                    name: "sala_atual",
                    label: "Sala atual"
                }),
                new TextInputWithButton({
                    name: "id",
                    label: "Número",
                    button_class: "ui-btn-c ui-icon-info"
                }),
                new SelectMenu({
                    name: "situacao",
                    label: "Situação",
                    options: situacao_opts
                }),
                new RadioGroup({
                    name: "etiqueta",
                    label: "Etiqueta",
                    options: etiqueta_opts
                })
            ],
            onSubmit: this.coletarSubmit.bind(this)
        })
        this.children = [this.form]
    }

    coletarSubmit(e) {
        e.preventDefault()
        const $infoBtn = $(e.target).find("#field_id button")
                                    .unbind("click")
                                    .removeClass("ui-btn-b ui-btn-d ui-btn-e")
                                    .addClass("ui-btn-c")
        let {id, ...values} = this.form.model.attributes
        if(!id) return this.numeroFocus()
        let model = new Inventario({id})
        model.fetch()
        BackboneAsync(model, this).then(() => {
            if(model.hasChanged()) {
                model.changed = {}
                let {id, sala_atual} = model.attributes
                this.lastId = id
                $infoBtn.bind("click", this.infoBtnClick(id))
                if(sala_atual) {
                    $infoBtn.removeClass("ui-btn-c").addClass("ui-btn-e")
                    const msg = `${id} já econtrado em ${sala_atual}`
                    console.debug(msg)
                    $.mobile.toast({message: msg})
                    return
                } else {
                    return this.updateModel(model, values).then(() => {
                        this.form.model.set("id", "")
                        $infoBtn.removeClass("ui-btn-c").addClass("ui-btn-b")
                    })
                }
            } else {
                $infoBtn.removeClass("ui-btn-c").addClass("ui-btn-d")
                $.mobile.toast({message: "Item não encontrado"})
                return
            }
        }).then(() => this.numeroFocus())
    }

    async updateModel(model, values) {
        // copiar valores do form
        Object.keys(values).forEach(key => {
            model.set(key, values[key])
        })
        model.save()
        return BackboneAsync(model, this).then(() => {
            const msg = `${model.id} econtrado`
            console.debug(msg)
            $.mobile.toast({message: msg})
        })
    }

    numeroFocus() {
        $("input[name=id]").get(0).focus()
    }

    infoBtnClick(id) {
        return () => {
            window.location.href = `#editar/${id}`
        }
    }
}
