import Backbone from 'backbone'
import Panel from "../components/Panel"
import SalaFilterDialog from '../components/SalaFilterDialog'
import ResponsaveisPopupMenu from '../components/ResponsaveisPopupMenu'

export default class FilterPanel extends Panel {
    get events() {
        return {
            "click #filter-list a[data-field]": this.filter.bind(this),
            "click #filter-list a[data-filter=clear]": this.clearFilter.bind(this),
            ...super.events
        }
    }
    constructor({parent, ...props}) {
        super({
            id: "filter-panel",
            display: "push",
            position: "right"
        })
        this.parent = parent
        this.children = [
            `<ul id="filter-list" data-role="listview" data-theme="c" style="margin-bottom: 16px">
                <li data-theme="a" data-role="list-divider">Localização</li>
                <li><a data-field="sala_atual" data-value="limpar">Tudo</a></li>
                <li><a data-field="sala_atual" data-value="nao-encontrados">Não encontrados</a></li>
                <li><a data-field="sala_atual" data-value="sala">Encontrado na sala</a></li>
                <li data-theme="a" data-role="list-divider">Responsavel pela busca</li>
                <li><a data-field="responsavel_busca" data-value="">Todos</a></li>
                <li><a data-field="responsavel_busca" data-value="selecionar">Selecionar</a></li>
                <li data-theme="a" data-role="list-divider">
                    <a data-filter="clear">Remover todos filtros</a>
                </li>
            </ul>`
        ]
    }

    async clearFilter(e) {
        this.parent.inventarios.clearFilter()
        Backbone.history.loadUrl()
    }

    async filter(e) {
        e.preventDefault()
        const field =  $(e.target).data("field")
        let value =  $(e.target).data("value")
        if(field == "sala_atual") {
            this.parent.inventarios.notFilterBy("sala_suap")
            switch(value) {
                case "nao-encontrados":
                    this.parent.inventarios.filterBy(field, null)
                    break
                case "sala":
                    let dialog = new SalaFilterDialog()
                    this.parent.inventarios.filterBy(field, await dialog.show())
                    break
                default:
                    this.parent.inventarios.notFilterBy(field)
            }
        }
        if(field == "responsavel_busca") {
            switch(value) {
                case "":
                    this.parent.inventarios.notFilterBy(field)
                    break
                default:
                    let popup = new ResponsaveisPopupMenu()
                    this.parent.inventarios.filterBy(field, await popup.show())
            }
        }
        Backbone.history.loadUrl()
    }
}
