import Page from '../../components/PageWithBackButton'
import PlanilhaConfig from '../../models/PlanilhaConfig'
import qrcode from 'qrcode-generator'

export default class ConfigURL extends Page {
	constructor() {
        super({title: 'URL de configuração'})
        this.planilhaConfig = new PlanilhaConfig()
        this.checkConfig()
    }

    checkConfig() {
        let config = null
        let configStr = window.location.href.split(/\?(.+)/, 2)[1]
        if(configStr) {
            try {
                config = JSON.parse(decodeURIComponent(configStr))
            } catch(ex) {
                console.error("URL de configuração incorreta: " + ex)
                return $.mobile.toast("URL de configuração incorreta")
            }                
        }
        if(config) {
            this.planilhaConfig.attributes = config
            this.planilhaConfig.save()
            window.router.navigate("/", {trigger: true, replace: true})
        } else {
            return this.getConfig()
        }
    }

    getConfig() {
        this.planilhaConfig.fetch()
        let currentURL = window.location.href.split(/\?(.+)/)[0]
        const configURL = currentURL + "?" + encodeURIComponent(JSON.stringify(
            this.planilhaConfig.attributes
        ))
        const qr = qrcode(0, 'M')
        qr.addData(configURL)
        qr.make()
        this.children = [
            `<img src="${qr.createDataURL()}" style="width: 100%">`,
            `<textarea>${configURL}</textarea>`
        ]
    }
}
