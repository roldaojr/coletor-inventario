import Page from '../../components/PageWithBackButton'
import ListView from '../../components/ListView'
import AsyncCollection from '../../models/AsyncCollection'
import PlanilhaConfig from '../../models/PlanilhaConfig'
import AsyncIndicator from '../../services/AsyncIndicator'

export default class SelecionarArquivo extends Page {
    get events() { return {
        "click #back-button": () => window.history.go(-1),
        'click .ui-listview a': "itemClick"
    }}

    constructor() {
        super({title: "Selecionar arquivo"})
        let itemTemplate = (item) => {
            return `<a id="${item.id}">${item.name}</a>`
        }
        this.planilhaConfig = new PlanilhaConfig()
        this.collection = new AsyncCollection(this.planilhaConfig.fetch_arquivos())
        this.children = [
            new ListView({collection: this.collection, itemTemplate})
        ]
        AsyncIndicator(new Promise(resolve => {
            this.listenTo(this.collection, "sync error", () => {
                resolve()
            })
        }))
    }

    itemClick(e) {
        this.planilhaConfig.set('spreadsheetId', e.target.id)
        this.planilhaConfig.save()
        window.history.go(-1)
    }
}
