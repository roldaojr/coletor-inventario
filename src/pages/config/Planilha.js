import Page from '../../components/PageWithMenu'
import Form from '../../components/Form'
import TextInput from '../../components/forms/TextInput'
import SelectMenu from '../../components/forms/SelectMenu'
import PlanilhaConfig from '../../models/PlanilhaConfig'
import AsyncCollection from '../../models/AsyncCollection'
import AsyncIndicator from '../../services/AsyncIndicator'

export default class ConfigPlanilha extends Page {
	get events() { return {
		"click #signin-btn": () => {
			this.planilhaConfig.authorize().then(auth => {
				return auth.signIn()
			}).catch(err => {
				console.error(err.error)
				$.mobile.toast({message: "Problemas ao fazer login no Google"})				
			})
		},
		"click #signout-btn": () => {
			this.planilhaConfig.authorize().then(auth => auth.signOut())
		},
		...super.events
	} }

	constructor() {
		super({
			title: 'Config Planilha',
            rightButton: `
            <a href="#/config/planilha/url" id="share-config" data-icon="navigation" data-iconpos="notext" style="display: none">
                Compartilhar
            </a>`
		})
		this.planilhaConfig = new PlanilhaConfig()
		this.planilhas = new AsyncCollection()
		this.colunas = new AsyncCollection()
		let form = new Form({
			id: "configForm",
			model: this.planilhaConfig,
			onSubmit: this.saveConfig.bind(this),
			children: [
				new TextInput({
					label: "Arquivo", 
					name: "spreadsheetId",
					readonly: true
				}),
				new SelectMenu({
					label: "Planilha", 
					name: "sheet",
					options: this.planilhas,
					onChange: this.planilhaChange.bind(this)
				}),
				new SelectMenu({
					label: "Numero", 
					name: "numero",
					options: this.colunas
				}),
				new SelectMenu({
					label: "Descrição", 
					name: "descricao",
					options: this.colunas
				}),
				new SelectMenu({
					label: "Sala SUAP", 
					name: "sala_suap",
					options: this.colunas
				}),
				new SelectMenu({
					label: "Carga SUAP", 
					name: "carga_suap",
					options: this.colunas
				}),
				new SelectMenu({
					label: "Responsável pela busca", 
					name: "responsavel_busca",
					options: this.colunas
				}),
				new SelectMenu({
					label: "Sala Atual", 
					name: "sala_atual",
					options: this.colunas
				}),
				new SelectMenu({
					label: "Estado Atual", 
					name: "estado_atual",
					options: this.colunas
				}),
				new SelectMenu({
					label: "Etiqueta", 
					name: "etiqueta",
					options: this.colunas
				})
			]
		})
		form.$el.hide()
		this.children = [
			`<a class="ui-btn ui-btn-c ui-corner-all" id="signin-btn" style="display:none">
				Fazer login
			</a>`,
			`<a class="ui-btn ui-btn-c ui-corner-all" id="signout-btn" style="display:none">
				Sair
			</a>`,
			`<a class="ui-btn ui-btn-a ui-corner-all" id="selecionar-arquivo-btn"
			href="#config/planilha/arquivo" style="display: none">
				Selecionar arquivo
			</a>`,
			form
		]
		this.listenTo(this.planilhaConfig, "change:sheet", this.colunasOptions)
		this.listenTo(this.planilhaConfig, "change:isSignedIn", this.onSigninUpdate)
		this.listenTo(this.planilhaConfig, "sync", (planilhaConfig) => {
			console.debug(`Planilha ${this.planilhaConfig.isConfigured ? '' : 'não '}configurada.`)
			if(this.planilhaConfig.isConfigured) {
				console.log($("#share-config"))
				this.$el.find("#share-config").show()
			}
			AsyncIndicator(planilhaConfig.authorize()).catch(err => {
				console.error(err)
				$.mobile.toast({
					message: "Problemas ao acessar API Google",
					duration: 6000
				})
			})
		})
	}

	planilhaChange(e) {
		console.debug("Selecionado planilha " + e.target.value)
		this.planilhaConfig.set("sheet", e.target.value)
	}

	saveConfig(e) {
		e.preventDefault()
		this.planilhaConfig.save()
		$.mobile.toast({message: "Configuração salva."})
		window.history.back()
	}

	async planilhaOptions() {
		let planilhas = await this.planilhaConfig.getPlanilhas()
		if(!planilhas && this.planilhaConfig.attributes.sheet) {
			planilhas = [this.planilhaConfig.attributes.sheet]
		}
		this.planilhas.set([
			{key: "", value: "Selecionar planilha"},
			...(planilhas || []).map(it => { return{key: it, value: it}})
		])
	}

	async colunasOptions() {
		let colunas = await this.planilhaConfig.getColunas()
		if(!colunas && this.planilhaConfig.attributes) {
			const {spreadsheetId, sheets, ...colmap} = this.planilhaConfig.attributes
			colunas = Object.values(colmap)
		}
		this.colunas.set([
			{key: "", value: "Selecionar coluna"},
			...(colunas || []).map((it, i) => { return{key: i + 1, value: it}})
		])
	}

    async onSigninUpdate(isLoggedin) {
		console.debug(`Update UI for isLoggedin = ${isLoggedin}`)
		if(isLoggedin) {
			const user = await this.planilhaConfig.getUser()
			$("#signout-btn").text(`Sair de ${user.emailAddress}`)
			$("#signin-btn").hide()
			$("#signout-btn").show()
			$("#selecionar-arquivo-btn").show()
			$("#configForm").show()
			try {
				await AsyncIndicator(this.planilhaOptions())
				await AsyncIndicator(this.colunasOptions())
			} catch (err) {
				console.error(err)
			}
		} else {
			$("#signin-btn").show()
			$("#signout-btn").hide()
			$("#selecionar-arquivo-btn").hide()
			$("#configForm").hide()
		}
		$.mobile.loading("hide")
	}

    render() {
        super.render()
        this.planilhaConfig.fetch()
    }
}
