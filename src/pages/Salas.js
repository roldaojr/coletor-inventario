import Page from '../components/PageWithMenu'
import ListView from '../components/ListView'
import { Salas } from '../models/Sala'
import { Inventarios } from '../models/Inventario'
import { BackboneAsync } from '../services/AsyncIndicator'

export default class SalasPage extends Page {
    get events() { return {
        "click .sala": this.salaClick,
    } }
    constructor() {
        super({title: "Salas"})
        let itemTemplate = (item, idx) => `
            <a class="sala"><span class="sala-nome">${item.attributes.nome}</span><br>
            ${item.attributes.encontrados || 0} / ${item.attributes.total || 0}</a>`
        this.salas = new Salas()
        this.children = [new ListView({
            theme: 'c',
            collection: this.salas,
            itemTemplate: itemTemplate
        })]
        this.salas.fetch()
        BackboneAsync(this.salas, this)
    }

    salaClick(e) {
        const sala_suap = $(e.target).find(".sala-nome").text()
        if(sala_suap) {
            const inventarios = new Inventarios()
            inventarios.notFilterBy("sala_atual")
            inventarios.filterBy("sala_suap", sala_suap)
            Backbone.history.navigate("/", { trigger: true })
        }
    }
}
