import Page from '../components/PageWithBackButton'
import Form from '../components/Form'
import TextImput from '../components/forms/TextInput'
import TextArea from '../components/forms/TextArea'
import SelectMenu from '../components/forms/SelectMenu'
import RadioGroup from '../components/forms/RadioGroup'
import { Inventario } from '../models/Inventario'
import { BackboneAsync } from '../services/AsyncIndicator'

let Field = e => e
let situacao_opts = {
    'Bom': 'Bom',
    'Ocioso': 'Ocioso',
    'Recuperável': 'Recuperável',
    'Irrecuperável': 'Irrecuperável',
    'Antieconomico': 'Antieconomico'
}
let etiqueta_opts = {'Boa': 'Boa', 'Necessaria': 'Necessaria'}

export default class Editar extends Page {
    constructor(model) {
        super({title: "Editar"})
        this.model = new Inventario({id: model})
        this.children = [
            new Form({
                model: this.model,
                onSubmit: this.formSave.bind(this),
                children: [
                    new TextImput({
                        name: "numero",
                        label: "Número",
                        readonly: true
                    }),
                    new TextArea({
                        name: "descricao",
                        label: "Descrição",
                        readonly: true
                    }),
                    new TextImput({
                        name: "sala_suap",
                        label: "Sala SUAP",
                        readonly: true
                    }),
                    new TextArea({
                        name: "carga_suap",
                        label: "Carga SUAP",
                        readonly: true
                    }),
                    new TextImput({
                        name: "responsavel_busca",
                        label: "Responsável pela busca",
                        readonly: true
                    }),
                    new TextImput({
                        name: "sala_atual",
                        label: "Sala atual"
                    }),
                    new SelectMenu({
                        name: "estado_atual",
                        label: "Situação",
                        options: situacao_opts
                    }),
                    new RadioGroup({
                        name: "etiqueta",
                        label: "Etiqueta",
                        options: etiqueta_opts
                    })
                ]
            })
        ]
        this.model.fetch()
        BackboneAsync(this.model, this)
    }

    formSave(e) {
        e.preventDefault()
        $(e.target).serializeArray().forEach(it => {
            this.model.set(it.name, it.value)
        })
        this.model.save()
        BackboneAsync(this.model, this).then(() => {
            window.history.go(-1)
        })
    }
}
