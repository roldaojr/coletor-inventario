import Panel from '../components/Panel'
import ListView from '../components/ListView'

export default class NavPanel extends Panel {
    constructor() {
        super({
            id: "navpanel",
            display: "push"
        })
        this.children = [
            new ListView({
                itemTemplate: i => `<a href="${i.url}">${i.name}</a>`,
                items: [
                    {url: "#", name: "Itens"},
                    {url: "#salas", name: "Salas"},
                    {url: "#coletar", name: "Coletar"},
                    {url: "#config", name: "Configurações"},
                ]
            })
        ]
    }
}
