import Page from '../components/PageWithMenu'

export default class ConfigPage extends Page {
	constructor() {
		super({
            title: 'Politica de privacidade',
        })
		this.children = [
            `<p>Este aplicativo faz uso do serviço de planilha do Google para 
             receber informações armazendas em uma planilha.</p>
             <p>Este aplicativo não armazena nenhuma informação em servidores,
             todos os dados ficam armazenados no dispositivo do usuário.
             Portanto é de inteira responsabilidade do usuário a manutenção
             da privacidade dos mesmos.</p>`
        ]
    }
}
