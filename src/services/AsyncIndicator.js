export const AsyncIndicator = async (promise) => {
    const timeout = setTimeout(() => $.mobile.loading('show', {
        text: "Aguarde",
        textVisible: true
    }), 500)
    try {
        const result = await promise
        return result
    } catch(err) {
        console.error('Async error', JSON.stringify(err))
        $.mobile.toast({message: "Erro desconhecido"})
    } finally {
        clearTimeout(timeout)
        $.mobile.loading('hide')
    }
    return promise
}

export const BackboneAsync = (model, view) => {
    return AsyncIndicator(
        new Promise((resolve, reject) => {
            view.listenToOnce(model, "sync", resolve)
            view.listenToOnce(model, "error", (_, response) => {
                if(response.result) return reject(response.result.error)
                reject(response)
            })
        })
    )
}

export default AsyncIndicator
