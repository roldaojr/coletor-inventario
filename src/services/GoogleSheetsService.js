const API_KEY = "AIzaSyCfjVytbPCcusqJYJWd6izB8XGlsuQ0VVY"
const CLIENT_ID = "1062212843371-vgp49lflbbjokctgpmc348sti4kbpbb3.apps.googleusercontent.com"
const SCOPES = [
    'openid',
    'profile',
    'https://www.googleapis.com/auth/drive.metadata.readonly',
    'https://www.googleapis.com/auth/spreadsheets',
]
const DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/drive/v3/rest"]
const SHEETS_SYNC_PREFIX = "inventario.sync.googlesheets."

const indexToA1 = (column, row) => {
    var temp, letter = '';
    while (column > 0) {
        temp = (column - 1) % 26;
        letter = String.fromCharCode(temp + 65) + letter;
        column = (column - temp - 1) / 26;
    }
    return `${letter}${row + 1}`
}

const chunkRange = (sheet, firstCol, lastCol, total_rows, chunk_size) => {
    let ranges = []
    for(var i = 1; i <= total_rows; i += chunk_size) {
        const lastLow = Math.min(total_rows - 1, i + chunk_size - 1)
        const rangeStart = indexToA1(firstCol + 1, i)
        const rangeEnd = indexToA1(lastCol, lastLow)
        ranges.push(`${sheet}!${rangeStart}:${rangeEnd}`)
    }
    return ranges
}

export class GoogleSheetsService {
    get API_KEY() { return API_KEY }

    async getGapi() {
        if(window.gapi) return window.gapi
        const callbackName = '__googleApiOnLoadCallback';
        const clientURL = `https://apis.google.com/js/api.js?onload=${callbackName}`;
        return new Promise(resolve => {
            console.debug('Importing Google API script.')
            const scriptTag = document.createElement('script')
            scriptTag.src = clientURL
            scriptTag.setAttribute("async", "async")
            scriptTag.setAttribute("defer", "defer")
            window[callbackName] = () => {
                resolve(window.gapi)
            }
            document.body.appendChild(scriptTag)
        })
    }
    async getApiClient() {
        if(window.gapi && window.gapi.client) return window.gapi.client
        console.debug('Loading Google API client.')
        const gapi = await this.getGapi()
        return new Promise(resolve => {
            gapi.load('client:auth2', () => {
                resolve(gapi.client)
            })
        })
    }

    async authorize() {
        if(window.gapi && window.gapi.auth2) {
            console.debug('Using gapi.auth2.')
            return window.gapi.auth2.getAuthInstance()
        }
        console.debug("Authorize access")
        const client = await this.getApiClient()
        await client.init({
            apiKey: API_KEY,
            client_id: CLIENT_ID,
            discoveryDocs: DISCOVERY_DOCS,
            scope: SCOPES.join(' '),
            access_type: 'offline',
        })
        return window.gapi.auth2.getAuthInstance()
    }

    async getDriveApi() {
        if(window.gapi.client.drive) {
            console.debug("Using gapi.client.drive.")
            return window.gapi.client.drive
        }
        await this.authorize()
        console.debug("Loading Google Drive API.")
        return window.gapi.client.load('drive', 'v3').then(() => {
            return window.gapi.client.drive
        })
    }

    async getSheetsApi() {
        if(window.gapi && window.gapi.client && window.gapi.client.sheets) {
            console.debug("Using gapi.client.sheets.")
            return window.gapi.client.sheets
        }
        await this.authorize()
        console.debug("Loading Google Sheets API.")
        return window.gapi.client.load('sheets', 'v4').then(() => {
            return window.gapi.client.sheets
        })
    }

    async getUser() {
        const drive = await this.getDriveApi()
        const response = await drive.about.get({'fields': 'user'})
        return response.result.user
    }

    async listSpreadsheetFiles() {
        const drive = await this.getDriveApi()
        console.debug("Getting spreadsheets list.")
        let request = drive.files.list({
            fields: "nextPageToken, files(id, name)",
            pageSize: 10,
            q: "mimeType='application/vnd.google-apps.spreadsheet'",
        })
        return new Promise(resolve => request.execute(resolve))
    }

    async listFileSheets(spreadsheetId) {
        const sheets = await this.getSheetsApi()
        let response = await sheets.spreadsheets.get({
            spreadsheetId: spreadsheetId
        })
        return response.result.sheets.map(item => {
            return item.properties.title
        })
    }

    async getInfo(spreadsheetId) {
        if(!spreadsheetId) return {}
        const sheets = await this.getSheetsApi()
        let response = await sheets.spreadsheets.get({
            spreadsheetId: spreadsheetId
        })
        return response.result
    }

    async fetchCells(spreadsheetId, range, majorDimension) {
        if(!spreadsheetId) return []
        const sheets = await this.getSheetsApi()
        let response = await sheets.spreadsheets.values.get({
            majorDimension: majorDimension ? majorDimension : 'ROWS',
            spreadsheetId, range
        })
        if(response.result && response.result.values) {
            return response.result.values
        }
        return []
    }
}

export class GoogleSheetsPouchDbAdapter {
    constructor({database, api, spreadsheetId, sheetName, columnMap}) {
        this.database = database
        this.api = api
        this.spreadsheetId = spreadsheetId
        this.sheetName = sheetName
        this.columnMap = columnMap
        console.debug("GoogleSheetsPouchDbAdapter", spreadsheetId, sheetName, columnMap)
    }

    async fetchCells(range, majorDimension) {
        let response = await this.api.spreadsheets.values.get({
            majorDimension: majorDimension ? majorDimension : 'ROWS',
            spreadsheetId: this.spreadsheetId,
            range: range
        })
        if(response.result && response.result.values) {
            return response.result.values
        }
        return []
    }

    async fetchHeader() {
        const values = await this.fetchCells(`${this.sheetName}!1:1`, 'ROWS')
        if (values.length > 0) return values[0]
        return []
    }

    async fetchIndex() {
        const values = await this.fetchCells(`${this.sheetName}!A:A`, 'COLUMNS')
        if (values.length > 0) return values[0]
        return []
    }

    values2doc(row) {
        let doc = {}
        Object.entries(this.columnMap).forEach((col, idx) => {
            doc[col] = row[idx]
        })
        doc._id = doc.numero
        return doc
    }

    doc2values({_id, _rev, ...doc}, rowIndex) {
        let values = []
        Object.entries(this.columnMap).forEach(([col, idx]) => {
            values[idx] = doc[col]
        })
        let rowNum = rowIndex.indexOf(_id)
        if(rowNum > 0) {
            return {
                range: `${this.sheetName}!${rowNum+1}:${rowNum+1}`,
                values: [values],
                majorDimension: 'ROWS'
            }
        } else {
            return null
        }
    }

    async push() {
        console.debug("Enviando dados para a planilha.")
        let index = await this.fetchIndex()
        let last_seq = window.localStorage.getItem(`${SHEETS_SYNC_PREFIX}last_seq`) || 0
        let changes = await this.database.changes({
            since: last_seq,
            include_docs: true,
            deleted: false
        })
        var docs = changes.results.map(change => change.doc)
        let rows = docs.map(doc => this.doc2values(doc, index)).filter(v => v)
        const result = await this.api.spreadsheets.values.batchUpdate({
            spreadsheetId: this.spreadsheetId,
            valueInputOption: 'USER_ENTERED',
            data: rows
        })
        window.localStorage.setItem(`${SHEETS_SYNC_PREFIX}last_seq`, last_seq)
        return result
    }

    async saveToDatabase(rows) {
        const docs = rows.map(row => {
            let doc = {}
            Object.keys(this.columnMap).forEach(col => {
                doc[col] = row[this.columnMap[col]]
            })
            doc._id = doc.numero
            return doc
        }).filter(doc => doc._id != null)
        let dbDocs = {}
        const result = await this.database.allDocs({
            startKey: docs[0]._id,
            endKey: docs[docs.length-1]._id,
            include_docs: true
        })
        result.rows.forEach(row => dbDocs[row['id']] = row['doc'])
        const newDocs = docs.filter(doc => {
            if(!Object.keys(dbDocs).includes(doc._id)) return true
            doc._rev = dbDocs[doc._id]._rev
            return JSON.stringify(dbDocs[doc._id]) != JSON.stringify(doc)
        })
        console.debug(`Chunk of ${docs.length} docs received. ${newDocs.length} docs changed.`)
        return this.database.bulkDocs(newDocs)
    }

    async pull() {
        console.debug("Recebendo dados da planilha.")
        let index = await this.fetchIndex(this.spreadsheetid, this.sheetName)
        let cols = Object.values(this.columnMap)
        let firstCol = Math.min(...cols)
        let lastCol = Math.max(...cols)
        let ranges = chunkRange(this.sheetName, firstCol, lastCol, index.length, 500)
        for(let i in ranges) {
            console.debug(`Fetching range ${ranges[i]}.`)
            const rows = await this.fetchCells(ranges[i], 'ROWS')
            for(let i = 0; i < rows.length; i += 100) {
                await this.saveToDatabase(rows.slice(i, i + 100))
            }
        }
        let last_seq = (await this.database.info()).update_seq
        window.localStorage.setItem(`${SHEETS_SYNC_PREFIX}last_seq`, last_seq)
    }

    async sync() {
        return this.push().then(this.pull.bind(this))
    }

    async searchMetadataByKey(key, location) {
        return this.api.spreadsheets.developerMetadata.search({
            spreadsheetId: this.spreadsheetId,
            dataFilters: [
                {
                    developerMetadataLookup: {
                        metadataKey: key,
                        metadataLocation: location
                    }
                }
            ]
        }).then(res => {
            if(res.result.matchedDeveloperMetadata) {
                return res.result.matchedDeveloperMetadata.map(it => {
                    return it.developerMetadata
                })
            } else {
                return []
            }
        })
    }

    async setMetadataByKey(key, value, location) {
        return this.api.spreadsheets.batchUpdate({
            spreadsheetId: this.spreadsheetId,
            requests: [
                {
                    deleteDeveloperMetadata: {
                        dataFilter: {
                            developerMetadataLookup: {
                                metadataKey: key,
                                metadataLocation: location
                            }
                        }
                    }
                },
                {
                    createDeveloperMetadata : {
                        developerMetadata: {
                            metadataKey: key,
                            metadataValue: String(value),
                            visibility: "PROJECT",
                            location
                        }
                    }
                }
            ]
        })
    }

    async getLastSeq() {
        return this.searchMetadataByKey("last_seq", {
            locationType: "ROW",
            dimensionRange: {
                dimension: "ROWS",
                startIndex: 0,
                endIndex: 1
            }
        }).then(results => {
            if(results.length > 0) return results[0].metadataValue; else return 0
        })
    }

    async setLastSeq(value) {
        console.debug(`Update last seq to ${value}`)
        return this.setMetadataByKey("last_seq", value, {
            dimensionRange: {
                dimension: "ROWS",
                startIndex: 0,
                endIndex: 1
            }
        }).then(res => {
            console.log('setMetadataByKey', res.result)
        })
    }
}

const googleSheetsServiceInstance = new GoogleSheetsService()

export const getGoogleSheetsService = () => googleSheetsServiceInstance
