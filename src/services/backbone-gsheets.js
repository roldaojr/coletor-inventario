import {getGoogleSheetsService} from './GoogleSheetsService'

export const columnIndexToA1range = (index) => {
    var temp, letter = '';
    while (index > 0) {
        temp = (index - 1) % 26;
        letter = String.fromCharCode(temp + 65) + letter;
        index = (index - temp - 1) / 26;
    }
    return letter
}

const defaultColumnMap = (size) => {
    const colmap = {}
    for(var i = 1; i <= size; i++) {
        colmap[columnIndexToA1range(i)] = i
    }
    console.log(colmap)
    return colmap
}

class BackboneGSheetsAdapter {
    constructor(defaults) {
        this.defaults = defaults || {}
        this.gsheets = getGoogleSheetsService()
    }

    async sync(method, model, options) {
        options = {...this.defaults, ...options}
        console.debug('sync', method, model, options)
        model.trigger('request', model, options);
        let response
        try {
            await this.createQuerySheet(options)
            if (method === 'read' && !model.id) {
                response = await this.list(model, options)
            } else {
                response = await this[method](model, options)
            }
            console.debug('result', response)
            options.success && options.success(response)
            return response
        } catch(error) {
            console.debug('error', JSON.stringify(error))
            options.error && options.error(error)
        }
    }

    async list(model, options) {
        const header = await this.getHeader(options)
        const lastCol = columnIndexToA1range(header.length)
        // query values
        let formula
        if(options.formula) {
            formula = options.formula
        } else {
            let query = ["select *"]
            if(options.query) {
                query = [options.query]
            }
            if(options.limit) query.push(` limit ${options.limit}`)
            if(options.offset) query.push(` offset ${options.offset}`)
            formula = `query(${options.sheet}!A:${lastCol};"${query.join("")}")`
        }
        const values = await this.query(options.spreadsheetId, formula, lastCol)
        // convert to attributes
        return values.slice(1).map(row => {
            return this.valuesToAttributes(row, options)
        })
    }

    async read(model, options) {
        const header = await this.getHeader(options)
        const lastCol = columnIndexToA1range(header.length)
        const values = await this.query(
            options.spreadsheetId,
            `query(${options.sheet}!A:${lastCol};"select * where A = ${model.id}")`,
            lastCol
        )
        if(values[0] && values[1]) {
            return this.valuesToAttributes(values[1], options)
        }
        return {}
    }

    async create(model, options) {
        console.error("not implemented")
    }

    async update(model, options) {
        const rowResult = await this.query(
            options.spreadsheetId,
            `match(${model.id}; Itens!A:A; 0)`,
            'A'
        )
        const row = rowResult
        const range = `${options.sheet}!${row}:${row}`
        const values = [this.attributesToValues(model.attributes, options)]
        const sheets = await this.gsheets.getSheetsApi()
        const response = await sheets.spreadsheets.values.update({
            valueInputOption: 'USER_ENTERED',
            majorDimension: 'ROWS',
            spreadsheetId: options.spreadsheetId, range, values
        })
        return response
    }

    async patch(model, options) {
        return this.update(model, options)
    }

    async delete(model, options) {
        console.error("not implemented")
    }

    async query(spreadsheetId, formula, lastCol) {
        console.log(`query ${spreadsheetId}, ${formula}, ${lastCol}`)
        if(!spreadsheetId) return []
        const auth = await this.gsheets.authorize()
        const access_token = auth.currentUser.get().getAuthResponse().access_token
        const range = `backbone-query!A:${lastCol}`
        const params = {
            valueInputOption: "USER_ENTERED",
            includeValuesInResponse: true,
            alt: "json",
            key: this.gsheets.API_KEY,
            access_token
        }
        const response = await Backbone.ajax({
            method: "put",
            cache: false,
            contentType: "application/json",
            crossDomain: true,
            url: `https://content-sheets.googleapis.com/v4/spreadsheets/${spreadsheetId}/values/${encodeURIComponent(range)}?${Backbone.$.param(params)}`,
            data: JSON.stringify({
                majorDimension: 'ROWS',
                values: [ [`=${formula}`] ]
            })
        })
        // convert to attributes
        if(response.result && response.result.updatedData) {
            return response.result.updatedData.values
        }
        if(response.updatedData) {
            return response.updatedData.values
        }
        return []
    }

    async getHeader(options) {
        const headerSize = options.headerSize || 1
        const sheets = await this.gsheets.getSheetsApi()
        const response = await sheets.spreadsheets.values.get({
            majorDimension: 'ROWS', spreadsheetId: options.spreadsheetId,
            range: `${options.sheet}!${headerSize}:${headerSize}`
        })
        return response.result.values.slice(-1)[0]
    }

    async createQuerySheet(options) {
        if(window.querySheetCreated) return
        const sheets = await this.gsheets.getSheetsApi()
        try {
            await sheets.spreadsheets.batchUpdate({
                spreadsheetId: options.spreadsheetId,
                requests: [
                    {
                        addSheet: {
                            properties: {
                                title: "backbone-query",
                                sheetType: "GRID",
                                hidden: true,
                            }
                        }
                    }
                ]
            })
            console.debug(`backbone-qiery criado.`)
        } catch(response) {
            console.debug(response.result.error.message)
        }
        window.querySheetCreated = true
    }

    valuesToAttributes(values, options) {
        const attributes = {}
        if(values) {
            const colmap = options.columnMap || defaultColumnMap(values.length)
            Object.entries(colmap).forEach(([col, i]) => {
                attributes[col] = values[i - 1]
            })
            attributes.id = values[0]
        }
        return attributes
    }

    attributesToValues(attributes, options) {
        const colcount = Object.keys(attributes).length
        const colmap = options.columnMap || defaultColumnMap(colcount)
        const values = []
        Object.entries(colmap).forEach(([col, i]) => {
            values[i - 1] = attributes[col]
        })
        return values
    }

}

export const BackboneGsheets = {
    sync: (options) => {
        const adapter = new BackboneGSheetsAdapter(options)
        return adapter.sync.bind(adapter)
    }
}

export default BackboneGsheets
