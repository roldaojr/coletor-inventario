import OfflinePluginRuntime from 'offline-plugin/runtime'
import $ from "jquery"
import "./jqmconfig"
import "jquery.mobile"
import "jquery.mobile.toast"
import "jquery-mobile-flatui"
import "./styles.css"
import Backbone from "backbone"
import NavPanel from "./pages/NavPanel"
import ItensPage from "./pages/Listar"
import EditarPage from "./pages/Editar"
import ColetarPage from "./pages/Coletar"
import SalasPage from './pages/Salas'
import PlanilhaConfigPage from './pages/config/Planilha'
import PlanilhaConfigURLPage from './pages/config/PlanilhaURL'
import PlanilhaArquivoPage from './pages/config/PlanilhaArquivo'
import PoliticaPrivacidadePage from './pages/PoliticaPrivacidade'

$.mobile.toast.prototype.options.duration = 4000

class AppRouter extends Backbone.Router {
    constructor() {
        super({routes: {
            "": () => this.changePage(new ItensPage()),
            "editar/:id": (id) => this.changePage(new EditarPage(id)),
            "coletar": () => this.changePage(new ColetarPage()),
            "salas": () => this.changePage(new SalasPage()),
            "config": () => this.changePage(new PlanilhaConfigPage()),
            "config/planilha/arquivo": () => this.changePage(new PlanilhaArquivoPage()),
            "config/planilha/url": () => this.changePage(new PlanilhaConfigURLPage()),
            "privacidade": () => this.changePage(new PoliticaPrivacidadePage())
        }})
        this.firstPage = true
        this.navpanel = new NavPanel()
        $("body").append(this.navpanel.render().el)
    }

    changePage(page) {
        $(page.el).attr('data-role', 'page');
        page.render();
        $('body').append($(page.el));
        let transition = $.mobile.defaultPageTransition;
        // We don't want to slide the first page
        if (this.firstPage) {
            transition = 'none'
            this.firstPage = false
        }
        $.mobile.changePage($(page.el), {changeHash:false, transition: transition})
    }
}

export var router

$(document).ready(() => {
    window.router = new AppRouter()
    Backbone.history.start()
    OfflinePluginRuntime.install()
})
