import $ from 'jquery'

$(document).bind("mobileinit", function () {
    $.mobile.ajaxEnabled = false
    $.mobile.linkBindingEnabled = false
    $.mobile.hashListeningEnabled = false
    $.mobile.pushStateEnabled = false

    // Remove page from DOM when it's being replaced
    $.mobile.document.bind('pagehide', function (event, ui) {
        $(event.target).remove();
    });

    $.mobile.document.bind('pagechange', function (e) {
        window.scrollTo(0, 1)
    })
})
