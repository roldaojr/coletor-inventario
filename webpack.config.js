'use strict';
const path = require('path')
const ProvidePlugin = require('webpack').ProvidePlugin;
const HtmlWebpackPlugin = require('html-webpack-plugin')
const FaviconsWebpackPlugin = require('favicons-webpack-plugin')
const OfflinePlugin = require('offline-plugin');
const WebpackPwaManifest = require('webpack-pwa-manifest')

module.exports = {
  entry: {
    polyfills: './src/polyfills.js',
    index: './src/index.js'
  },
  output: {
    path: __dirname + '/dist',
    filename: '[name].bundle.js'
  },
  devServer: {
    contentBase: './dist',
    compress: true,
    disableHostCheck: true,
    hot: false,
    inline: false
  },
  devtool: 'eval-source-map',
  module: {
    rules: [
      {
        test: [
          /src\/.*\.js$/, /src\\.*\.js$/,
          /lib\/.*\.js$/, /lib\\.*\.js$/
        ],
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              ['@babel/preset-env', {
                targets: "android 2.3",
                useBuiltIns: false
              }]
            ],
            plugins: [
              "@babel/plugin-proposal-class-properties",
              "@babel/plugin-transform-reserved-words",
              "@babel/plugin-transform-member-expression-literals",
              "transform-es3-property-literals",
            ]
          }
        }
      },
      {
        test: [/node_modules\/.*\.js$/, /node_modules\\.*\.js$/],
        exclude: [
          /node_modules\/webpack/, /node_modules\\webpack/,
          /node_modules\/core-js/, /node_modules\\core-js/,
        ],
        use: {
          loader: 'babel-loader',
          options: {
            plugins: [
              "@babel/plugin-transform-reserved-words",
              "@babel/plugin-transform-member-expression-literals",
              "transform-es3-property-literals"
            ]
          }
        }
      },
      {
        test: /[\/\\]lib[\/\\]jquery\.mobile\.js$/,
        loader: "imports-loader?this=>window"
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(png|jpe?g|gif|svg|eot|ttf|woff|woff2)$/i,
        loader: 'file-loader',
        options: {
          limit: 8192,
        },
      }
    ]
  },
  resolve: {
    alias: {
      'backbone-pouch': path.resolve(__dirname, 'lib/backbone-pouch.js'),
      'jquery.mobile': path.resolve(__dirname, "lib/jquery.mobile"),
      'jquery.mobile.toast': path.resolve(__dirname, "lib/jquery.mobile.toast"),
      'jquery-mobile-flatui': path.resolve(__dirname, "lib/jquery-mobile-flatui/jquery.mobile.flatui.css")
    }
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './assets/index.html',
      filename: 'index.html'
    }),
    new ProvidePlugin({
      '$': 'jquery',
      'jQuery': 'jquery',
    }),
    new FaviconsWebpackPlugin("assets/logo.png"),
    new OfflinePlugin({
      responseStrategy: 'network-first'
    }),
    new WebpackPwaManifest({
      name: 'Inventario IFRN',
      short_name: 'Inventario',
      background_color: '#ffffff',
      icons: [
        {
          src: path.resolve('assets/logo.png'),
          sizes: [96, 128, 192, 256, 384, 512] // multiple sizes
        }
      ]
    })
  ],
  node: {
    __filename: true,
    __dirname: true,
  }
}
